using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Unity.VisualScripting;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private Transform playerTransform;
    [SerializeField] private float movementSpeed = 3.0f;
    private bool isFixed = false;
    private Transform targetTransform;
    private Camera cam;

    private float targetSize = 10f;
    private float resizeSpeed = 1f;

    private void Start()
    {
        cam = GetComponent<Camera>();
        playerTransform = GameObject.Find("Player").transform;
        transform.eulerAngles = new Vector3(30, 45, 0);
    }
    // Update is called once per frame
    void Update()
    {   
        transform.position = Vector3.Lerp(transform.position, (isFixed ? targetTransform.position : playerTransform.position) + new Vector3(-11,11,-11), movementSpeed * Time.deltaTime);
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, targetSize, resizeSpeed * Time.deltaTime);
    }

    public void EnterLevel(Transform target)
    {
        targetTransform = target;
        isFixed = true;

    }

    public void ExitLevel()
    {
        isFixed = false;
    }

    public void changeSize(float targetSize, float resizeSpeed)
    {
        this.targetSize = targetSize;
        this.resizeSpeed = resizeSpeed;
    }
}
