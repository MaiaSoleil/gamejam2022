using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    [SerializeField] AudioSource walk;
    [SerializeField] AudioSource pick;
    [SerializeField] AudioSource plug;
    [SerializeField] AudioSource teleport;
    [SerializeField] AudioSource throwAudio;
    [SerializeField] AudioSource music;

    static Dictionary<string, AudioSource> audios;

    public static void Play(string audio)
    {
        var a = audios[audio];
        a.Play(0);
    }

    //music by Dark Atmospheric Music from the Shadowlands: https://www.youtube.com/watch?v=000z5zd6mrc


    // Start is called before the first frame update
    void Start()
    {
        audios = new Dictionary<string, AudioSource>
    {
        {"walk" , walk},
        {"pick" , pick},
        {"plug" , plug},
        {"teleport" , teleport},
        {"throw" , throwAudio},
        {"music" , music},
    };
    }

    float timer = 0f;
    [SerializeField] float time_for_new_step = .1f;
    // Update is called once per frame
    void Update()
    {
        //timer += Time.deltaTime;

        //if (timer > time_for_new_step)
        //{
        //    timer = 0;
        //    if (Input.GetAxis("Horizontal") > 0 || Input.GetAxis("Vertical") > 0)
        //    {
        //        SoundManager.Play("walk");
        //    }
        //}
        
    }
}
