using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableThrow : MonoBehaviour
{
    [SerializeField] private Transform startTransform;
    [SerializeField] private Transform targetTransform;
    [SerializeField] private float throwSpeed = 3.0f;
    Vector3 targetPosition;
    // Start is called before the first frame update
    void Start()
    {

        targetPosition = targetTransform.position;
        transform.position = startTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, targetPosition, throwSpeed * Time.deltaTime);
    }

    private void OnApplicationQuit()
    {
        transform.position = Vector3.zero;
    }
}
