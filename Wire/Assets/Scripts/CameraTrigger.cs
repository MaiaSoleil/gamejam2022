using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrigger : MonoBehaviour
{

    private CameraManager cameraManager;
    [SerializeField] GameObject target;
    [SerializeField] float size = 10f;
    [SerializeField] float resizeSpeed = 1f;
    private Transform playerTransform;

    [SerializeField] private bool debug = true; 
    // Start is called before the first frame update
    void Start()
    {
        cameraManager = Camera.main.GetComponent<CameraManager>();
        playerTransform = GameObject.Find("Player").transform;
        GetComponent<MeshRenderer>().enabled = debug;
        target.GetComponent<MeshRenderer>().enabled = debug;
        target.transform.position = new Vector3(target.transform.position.x, playerTransform.position.y, target.transform.position.z);

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.gameObject.CompareTag("Player"))
        {
            cameraManager.EnterLevel(target.transform);
            cameraManager.changeSize(size, resizeSpeed);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.gameObject.CompareTag("Player"))
        {
            cameraManager.ExitLevel();
        }
    }
}
