using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class VictimController : MonoBehaviour
{

    public GameObject plug;
    public GameObject victim;


    public void Free()
    {
        StartCoroutine(Animate());
    }

    private IEnumerator Animate()
    {
        print(1);
        StartCoroutine(MoveOverSeconds(plug, plug.transform.position + plug.transform.forward * 0.6f, 1));
        yield return new WaitForSeconds(1);
        StartCoroutine(MoveOverSeconds(plug, plug.transform.position + plug.transform.up * -2f, 1));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(MoveOverSeconds(victim, victim.transform.position + plug.transform.up * -0.8f, 2));
        yield return new WaitForSeconds(2);
        victim.GetComponent<Animator>().SetBool("Free", true);
    }

    private IEnumerator MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while(elapsedTime < seconds)
        {
            objectToMove.transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        objectToMove.transform.position = end;
    }
}
