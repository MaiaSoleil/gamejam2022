using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SplineMesh;

public class Teleportation : MonoBehaviour
{
    private Transform child1;
    private Transform child2;
    private Transform plug1;
    private Transform plug2;

    private Spline spline;
    private float t = 0;
    public float numberOfSegments = 0;
    private bool animate = false;
    private bool fromStart = true;

    [SerializeField]
    private GameObject particlesRef;
    private GameObject particles;
    private GameObject pipe;
    // Start is called before the first frame update
    void Start()
    {
        plug1 = transform.Find("Plug1");
        plug2 = transform.Find("Plug2");
        child1 = plug1.Find("Teleport1");
        child2 = plug2.Find("Teleport2");

        pipe = transform.Find("Corde").gameObject;
        spline = pipe.GetComponent<Spline>();
        numberOfSegments = spline.curves.Count;

    }

    
    //Return position and rotation of player. return value: x = x position, y = y rotation, z = z position
    public Vector3 TeleportPosition(GameObject other, Vector3 playerPosition){
         
        if(other.name.Substring(0,9) == child1.name){
            fromStart = false;
            return new Vector3(child2.position.x, plug2.eulerAngles.y, child2.position.z);
        }else if(other.name.Substring(0,9) == child2.name){
            fromStart = true;
            return new Vector3(child1.position.x, plug1.eulerAngles.y, child1.position.z);
        }

        return playerPosition;
    }
    private bool firstLocation;

    public void Animate()
    {
        animate = true;
        firstLocation = true;
        t = fromStart ? 0 : numberOfSegments;
    }

    // Update is called once per frame
    void Update()
    {
        if (!animate)
            return;

        if (fromStart && t > numberOfSegments || !fromStart && t < 0)
        {
            animate = false;
            if(particles)
            {
                Destroy(particles);
            }
            return;
        }

        var curve = spline.GetCurve(t);

        var location = curve.GetLocation(t - Mathf.Floor(t));
        if (firstLocation)
        {
            firstLocation = false;
            particles = Instantiate(particlesRef);
        }
        particles.transform.position = location + transform.position;
        
        t += (fromStart ? 1 : -1) * Time.deltaTime;
    }
}
