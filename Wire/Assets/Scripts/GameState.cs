using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;

public class GameState : MonoBehaviour
{
    // Put into this all victim instances
    public List<GameObject> victims;

    private bool[] freedVictims;
    private PlayerController playerController;

    private void Start()
    {
        freedVictims = Enumerable.Repeat(false, victims.Count).ToArray();
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    public bool freeVictim(GameObject victimObject)
    {
        if (!victims.Contains(victimObject))
        {
            throw new MissingComponentException("Victim is not registered in the game state");
        }
        int victimIndex = victims.IndexOf(victimObject);
        if (freedVictims[victimIndex])
        {
            return false;
        }
        freedVictims[victimIndex] = true;

        notifyPlayer();
        return true;
    }

    private void notifyPlayer()
    {
        // Send player controller how many victims are freed
        playerController.attemptLevelUp(freedVictims.Count(x => x));
    }

}
