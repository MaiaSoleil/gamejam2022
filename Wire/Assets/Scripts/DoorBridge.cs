using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBridge : MonoBehaviour
{
    public GameObject safeWalls;
    public Vector3 closedPosition;
    public Vector3 openedPosition;
    public bool isOpen = false;
    public float openSpeed = 3f;
    public float closeSpeed = 3f;

    private bool isInTransit = false;

    private Coroutine transitCoroutine;

    public void open()
    {
        if (isInTransit)
        {
            StopCoroutine(transitCoroutine);
        }
        isInTransit = true;
        transitCoroutine = StartCoroutine(MoveOverSpeed(gameObject, openedPosition, openSpeed));
        isOpen = true;
    }

    public void close()
    {
        Debug.Log("DoorOpenClose");
        if (isInTransit)
        {
            StopCoroutine(transitCoroutine);
        }
        isInTransit = true;
        transitCoroutine = StartCoroutine(MoveOverSpeed(gameObject, closedPosition, closeSpeed));
        isOpen = false;
    }

    public void flip()
    {
        if (isOpen)
        {
            close();
        }
        else
        {
            open();
        }
    }


    private IEnumerator MoveOverSpeed(GameObject objectToMove, Vector3 end, float speed)
    {
        while(objectToMove.transform.position != end)
        {
            objectToMove.transform.localPosition = Vector3.MoveTowards(objectToMove.transform.localPosition, end, speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        isInTransit = false;
        safeWalls.SetActive(!isOpen);
    }
}
