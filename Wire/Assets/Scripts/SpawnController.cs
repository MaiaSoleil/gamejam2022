using System.Collections;
using UnityEngine;

public class SpawnController : MonoBehaviour
{

    public GameObject plug;
    public GameObject victim;
    public GameObject explosionPrefab;
    public RuntimeAnimatorController playerAnimator;


    private void Start()
    {

        StartCoroutine(Animate());
    }

    private IEnumerator Animate()
    {
        yield return new WaitForSeconds(5);
        StartCoroutine(Explode());
        yield return new WaitForSeconds(5);
        StartCoroutine(MoveOverSeconds(plug, plug.transform.position + plug.transform.forward * 0.6f, 1));
        yield return new WaitForSeconds(1);
        StartCoroutine(MoveOverSeconds(plug, plug.transform.position + plug.transform.up * -2f, 1));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(MoveOverSeconds(victim, victim.transform.position + plug.transform.up * -0.8f, 2));
        yield return new WaitForSeconds(2);
        victim.GetComponent<Animator>().SetBool("Free", true);
        yield return new WaitForSeconds(2);
        victim.GetComponent<Animator>().runtimeAnimatorController = playerAnimator;
        victim.GetComponent<Rigidbody>().isKinematic = false;
        victim.GetComponent<PlayerController>().enabled = true;
        victim.transform.parent = null;

    }

    private IEnumerator MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while(elapsedTime < seconds)
        {
            objectToMove.transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        objectToMove.transform.position = end;
    }

    private IEnumerator Explode()
    {
        Vector3[] explosionsPos = new Vector3[10] {
            new Vector3(0, 1, 0),
            new Vector3(0, 1, 1),
            new Vector3(1, 1, 0),
            new Vector3(0, 1, -1),
            new Vector3(-1, 1, 0),
            new Vector3(1, 1, 1),
            new Vector3(-1, 1, -1),
            new Vector3(-1, 1, 1),
            new Vector3(1, 1, -1),
            new Vector3(1, 1, 1),
        };
        for(int i = 0; i < 4; i++)
        {
            foreach (Vector3 pos in explosionsPos)
            {
                StartCoroutine(SpawnExplosion(pos));
                yield return new WaitForSeconds(0.3f);
            }
        }
    }

    private IEnumerator SpawnExplosion(Vector3 offset)
    {
        GameObject exp = Instantiate(explosionPrefab, offset, Quaternion.identity);
        yield return new WaitForSeconds(1);
        Destroy(exp);
    }
}
