using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wire : MonoBehaviour
{
    public GameObject otherSide;
    public bool isPlugged = false;
    public GameObject plug = null;
    public int side;
}
