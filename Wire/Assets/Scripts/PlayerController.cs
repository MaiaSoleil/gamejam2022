using System.Collections;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [Range(0.01f, 0.2f)] public float moveSpeed = 0.12f;
    public KeyCode interactKey = KeyCode.Z;

    public Vector3 batteryOffset;
    public Quaternion batteryOffAngle;

    // Subject to change si on veut polymorphiser
    private GameObject interactingObject = null;
    private GameObject directionArrow = null;

    private Rigidbody rb;
    private GameState gameState;
    private Animator anim;

    private string[] InteractibleTags = new string[] { "InteractVictim", "InteractYellow", "InteractPlogue", "InteractThrowingPlug" };

    private bool moveLocked = false;
    private bool holdingPlug = false;

    public float throwForce = 400f;


    private Teleportation teleportParent;
    private GameObject handheld;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        gameState = GameObject.Find("GameState")?.GetComponent<GameState>();
        directionArrow = GameObject.FindGameObjectWithTag("DirectionalArrow");
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        move();
        interact();
    }

    public void attemptLevelUp(int freedAmount)
    {
        print($"Attempting level up with {freedAmount} free people.");
    }

    // Is called when collider enters trigger
    private void OnTriggerEnter(Collider other)
    {
        if (InteractibleTags.Contains(other.tag))
        {
            interactingObject = other.gameObject;
        }
    }

    // Is called when collider leaves trigger
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == interactingObject)
        {
            interactingObject = null;
        }
    }

    private void move()
    {
        if (!moveLocked)
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            Vector3 movementCap = Quaternion.Euler(0, 45, 0) * new Vector3(x, 0, z).normalized;
            Vector3 movement = Quaternion.Euler(0, 45, 0) * new Vector3(x, 0, z);
            if(movement.magnitude > movementCap.magnitude)
            {
                movement = movementCap;
            }
            if (movement == Vector3.zero)
            {
                anim.SetBool("Walking", false);
                return;
            }
            anim.SetBool("Walking", true);
            anim.SetFloat("WalkSpeed", movement.magnitude * moveSpeed);
            transform.forward = movement;
            rb.MovePosition(transform.position + movement * moveSpeed);
        }
        else
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            if (x == 1 && z == 1 || x == 0 && z == 0)
            {
                rb.transform.eulerAngles = new Vector3(0f, 90f, 0f);
                directionArrow.transform.eulerAngles = new Vector3(90f, 90f, 0f);
                directionArrow.transform.position = rb.transform.position + new Vector3(1.5f, 1f, 0f);
            }
            else if (x == -1 && z == 1)
            {
                rb.transform.eulerAngles = new Vector3(0f, 0f, 0f);
                directionArrow.transform.eulerAngles = new Vector3(90f, -180f, 0f);
                directionArrow.transform.position = rb.transform.position + new Vector3(0, 1f, 1.5f);
            }
            else if (x == -1 && z == -1)
            {
                rb.transform.eulerAngles = new Vector3(0f, -90f, 0f);
                directionArrow.transform.eulerAngles = new Vector3(90f, -90f, 0f);
                directionArrow.transform.position = rb.transform.position + new Vector3(-1.5f, 1f, 0f);

            }
            else if (x == 1 && z == -1)
            {
                rb.transform.eulerAngles = new Vector3(0f, 180f, 0f);
                directionArrow.transform.eulerAngles = new Vector3(90f, 180f, 0f);
                directionArrow.transform.position = rb.transform.position + new Vector3(0, 1f, -1.5f);
            }
        }

    }

    private void interact()
    {
        if (Input.GetKeyDown(interactKey) && interactingObject != null)
        {
            switch (interactingObject.tag)
            {
                case "InteractVictim":
                    interactVictim();
                    break;
                case "InteractYellow":
                    teleport(interactingObject);
                    break;
                case "InteractPlogue":
                    plogue();
                    break;
                case "InteractThrowingPlug":
                    interactThrowingPlug();
                    break;
                default:
                    print("Found undefined interactible tag");
                    break;
            }
        }
    }

    private void interactVictim()
    {
        print("Interacting with victim");
        GameObject victim = interactingObject.transform.parent.gameObject;
        bool canFree = gameState.freeVictim(victim);
        if (canFree)
        {
            victim.GetComponent<VictimController>().Free();
        }
    }

    private void teleport(GameObject teleportObj){

        teleportParent = teleportObj.transform.parent.parent.GetComponent<Teleportation>();
        Vector3 pos = teleportParent.TeleportPosition(teleportObj, transform.position);
        teleportParent.Animate();
        StartCoroutine(teleportAction(teleportParent.numberOfSegments, pos));
    }

    IEnumerator teleportAction(float t, Vector3 pos)
    {
        SoundManager.Play("teleport");
        transform.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
        yield return new WaitForSeconds(t);
        transform.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
        transform.position = new Vector3(pos.x, transform.position.y, pos.z);
        transform.rotation = Quaternion.Euler(transform.rotation.x, pos.y + 180, transform.rotation.z);
        SoundManager.Play("teleport");
    }  

    private void plogue()
    {
        // TMP jusqu'� la logique de la batterie
        print("interacting with plogue");
        
        Plogue plogue = interactingObject.GetComponentInParent<Plogue>();
        print(interactingObject.name[9]);
        int side = interactingObject.name[9] - '0';
        if(handheld == null)
        {
            handheld = plogue.Unplug(side);
            SoundManager.Play("pick");

            if (handheld != null)
            {
                anim.SetBool("Holding", true);
                handheld.transform.SetParent(transform, true);
                handheld.transform.localPosition = batteryOffset;
                handheld.transform.localRotation = batteryOffAngle;
            }
        }
        else
        {
            try
            {
                plogue.Plug(handheld, side);
                SoundManager.Play("plug");

            }
            catch
            {
                return;
            }
            handheld = null;
            anim.SetBool("Holding", false);
        }
    }

    private void interactThrowingPlug()
    {
        if (!holdingPlug) {
            rb.transform.position = new Vector3(interactingObject.transform.position.x, 0, interactingObject.transform.position.z);

            holdingPlug = true;
            moveLocked = true;
            directionArrow.SetActive(true);
            directionArrow.GetComponent<MeshRenderer>().enabled = true;
            directionArrow.transform.eulerAngles = new Vector3(rb.transform.forward.x + 90f, rb.transform.forward.y, rb.transform.forward.z );
            directionArrow.transform.position = rb.transform.position + new Vector3(0, 1.5f, 1.5f);
        }
        else
        {
            Vector3 playerForwardVector = rb.transform.forward;
            Rigidbody plugRigidbody = interactingObject.GetComponent<Rigidbody>();
            plugRigidbody.AddForce(playerForwardVector * throwForce);
            directionArrow.GetComponent<MeshRenderer>().enabled = false;
            directionArrow.SetActive(false);

            StartCoroutine(UnlockMoveCoroutine());
        }

    }

    IEnumerator UnlockMoveCoroutine()
    {
        SoundManager.Play("thow");
        yield return new WaitForSeconds(1);
        moveLocked = false;
        holdingPlug = false;
    }
}
