using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Plogue : MonoBehaviour
{
    public GameObject mechanism;
    public bool isPowered = false;
    public GameObject[] pluggedObjects = new GameObject[4];
    public Vector3 offset;
    public Quaternion offAngle;

    private DoorBridge doorBridge;

    private void Start()
    {
        if(mechanism != null)
        {
            doorBridge = mechanism.GetComponent<DoorBridge>();
        }
        refreshPower();
    }

    public void Plug(GameObject o, int side)
    {
        if (pluggedObjects[side] != null)
        {
            throw new UnityException("Can't plug in occupied socket");
        }
        pluggedObjects[side] = o;
        Transform pluggedTrans = o.transform;
        pluggedTrans.position = transform.position + offset;
        pluggedTrans.rotation = offAngle;
        pluggedTrans.RotateAround(transform.position, Vector3.up, 90 * side);
        pluggedTrans.transform.SetParent(transform, true);
        refreshPower();
    }

    public GameObject Unplug(int side)
    {
        //TODO
        GameObject o = pluggedObjects[side];
        if(o == null)
        {
            return null;
        }
        pluggedObjects[side] = null;
        refreshPower();
        return o;
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "InteractThrowingPlug" && !other.gameObject.GetComponent<Wire>().isPlugged)
        {
            other.gameObject.GetComponent<Wire>().isPlugged = true;
            other.gameObject.GetComponent<Wire>().plug = gameObject;
            other.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

            if (other.gameObject.GetComponent<Wire>().otherSide.GetComponent<Wire>().isPlugged) doorBridge?.open();
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "InteractThrowingPlug" && other.gameObject.GetComponent<Wire>().isPlugged)
        {
            doorBridge?.close();

            //TODO :: other.gameObject.GetComponent<Wire>().isPlugged = false;

            other.gameObject.GetComponent<Wire>().plug = null;
        }
    }

    private bool isBatteryPlugged()
    {
        bool value = false;
        for (int i = 0; i < pluggedObjects.Length; i++)
        {
            if (pluggedObjects[i] != null && pluggedObjects[i].GetComponent<Wire>() == null)
            {
                value = true;
            }
        }
        return value;
    }

    private void refreshPower()
    {
        if(isBatteryPlugged() && pluggedObjects.Count(x=>x!=null) > 0)
        {
            doorBridge?.open();
        }
        else
        {
            doorBridge?.close();
        }
    }
}
